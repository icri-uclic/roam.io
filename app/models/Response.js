const mongoose = require('mongoose');

const schema = mongoose.Schema({
  time: {
    type: Date,
    default: Date.now,
    index: true
  },
  language: String,
  question: String,
  questionId: String,
  answer: String,
  answerIndex: Number,
  custom: Boolean,
  dwell: Number
});

module.exports = mongoose.model("Response", schema);
