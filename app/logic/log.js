const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const logDebug = require('debug')('roamio:logging');
const config = require('../../config');
const { datestamp } = require('./utils');

const filename = datestamp() + ".log";

config.LOGS_FOLDERS.forEach((folder) => mkdirp.sync(folder));

const files = config.LOGS_FOLDERS.map((folder) => {
  return fs.openSync(
    path.resolve(path.join(folder, filename)),
    'a'
  );
});

function stringify(x) {
  return _.isString(x) ? x : JSON.stringify(x);
}

function log(...args) {
  const message = args.map(stringify).join(" ");
  files.forEach((file) => {
    fs.appendFile(file, message + "\n", function(error) {
      if (error) return logDebug("Failed to append log to file.", error);
    });
  });
}

module.exports = function(type) {
  return function(...args) {
    const time = new Date().toISOString();
    log(time, type, ...args);
  }
};
