const debug = require('debug')('roamio:state');
const logEvent = require('../log')('state');
const { eventNames } = require('../utils');

module.exports = function(state) {
  logEvent("begin", state.getState());
  eventNames.forEach((event) => {
    state.on(event, (arg) => {
      debug(event, arg);
      logEvent(event, arg);
    });
  });
}
