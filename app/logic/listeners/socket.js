/**
  STATE ⟶ SOCKET
*/

const config = require('../../../config');
const { eventNames } = require('../utils');

exports.fromState = function(state, socket) {

  eventNames.forEach((event) => {
    state.on(event, (...args) => {
      socket.emit(event, ...args);
    });
  });

  state.on("update:language", function(lang) {
    socket.emit("update:placeholder", config.KEYBOARD_EXPLANATION[lang]);
  });
}


/**
  SOCKET ⟶ STATE
*/

exports.toState = function(state, socket) {

  socket.on("edit:answer", ({ index, value }) => {
    let answers = state.get("answers");
    let answer = answers[index];
    if (answer) {
      answer.text = value;
      state.set("answers", answers);
    }
  });

}
