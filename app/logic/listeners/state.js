const _ = require('lodash');
const path = require('path');
const applescript = require('applescript');
const debug = require('debug')("roamio:error");
const logDebug = require('debug')('roamio:logging');

const { stripHTML } = require('../utils');
const nextQuestion = require('../next-question');
const config = require('../../../config');
const responses = require('../responses');

// Stores the absolute time when question appears, so that the dwell time can be
// calculated afterwards.
let dwellTimeBegin;

// milliseconds to wait after question appears to show answers
const ANSWERS_DELAY = 1000;

function propsFromQuestion(question, lang) {
  const props = {
    question: {
      text: question.question[lang]
    },
    answers: question.answers.map((ans) => ({
      text: ans.answer[lang],
      keyboard: ans.keyboard
    }))
  };
  if (question.visualisation && question.visualisation.type) {
    props.visualisation = question.visualisation;
  }
  return props;
}

function getTotalCount(counts) {
  let total = 0;
  for (const key in counts) {
    total += parseInt(counts[key], 10);
  }
  return total;
}

module.exports = function(state, firebase) {

  state.on("update:state", (newState) => {

    if (newState === "BEGIN") {

      const question = nextQuestion();

      state.update({
        visualisation: {},
        originalQuestion: question,
        question: {
          text: config.GREETING[state.get("language")]
        },
        answers: [],
        keyboard: null
      });

    } else if (newState === "QUESTION") {

      const question = state.get("originalQuestion");
      const lang = state.get("language");
      const props = propsFromQuestion(question, lang);
      const answers = props.answers;
      delete props.answers;
      state.update(props);

      setTimeout(() => {
        state.set("answers", answers);
        dwellTimeBegin = Date.now();
      }, ANSWERS_DELAY);

    } else if (newState === "RESPONSE") {

    }
  });

  state.on("select:answer", (index) => {

    const question = state.get("originalQuestion");
    const answer = state.get("answers")[index];
    const lang = state.get("language");

    let r = {
      time: new Date(),
      language: lang,
      questionId: question.id,
      question: stripHTML(state.get("question").text),
      answer: stripHTML(answer.text),
      answerIndex: index,
      custom: answer.keyboard || false,
      dwell: Date.now() - dwellTimeBegin
    };

    responses.save(r);

    try {
      let firebaseR = Object.assign({}, r);
      firebaseR.time = r.date.valueOf()
      firebase.database().ref("/responses/").push(firebaseR, function(){
        logDebug("updated on firebase")
      });

    } catch (e) {
      debug(e)
    }

    state.update({
      state: "RESPONSE",
      answers: [],
      keyboard: null
    });

    let response = _.get(question, `answers[${index}].response[${lang}]`);

    if (response != null) {
      state.set("question", { text: response });
    }

    responses.getCounts(question.id).then((counts) => {

      const total = getTotalCount(counts);

      let showExplanation = true;

      if (response == null) {
        showExplanation = false;
        const count = counts[index];
        const fraction = ((count/total) * 100);
        let response;
        if (!isNaN(fraction) && isFinite(fraction)) {
          const percent = fraction.toFixed(0);
          response = config.DEFAULT_RESPONSE[lang](percent);
        } else {
          response = config.DEFAULT_RESPONSE.DEFAULT[lang];
        }
        state.set("question", { text: response });
      }

      if (question.visualisation.show_responses && total >= config.MINIMUM_RESPONSES) {
        const lang = state.get("language");
        state.set("visualisation", {
          type: "responses",
          heading: config.RESPONSES_HEADING[lang],
          explanation: showExplanation,
          data: _.map(counts, function(count, index) {
            return {
              label: question.answers[index].answer[lang],
              count: count
            };
          })
        });
      }

    }).catch((error) => debug(error));

    setTimeout(() => {
      state.set("state", "BEGIN");
    }, question.timeout);

  });

  state.on("update:language", (lang) => {
    const currentState = state.get("state");
    const question = state.get("originalQuestion");
    if (currentState === "BEGIN") {
      state.set("question", { text: config.GREETING[lang] });
    } else if (currentState === "QUESTION") {
      state.set("keyboard", null);
      state.update(propsFromQuestion(question, lang));
    }
  });

  state.on("update:keyboard", (focus) => {
    if (focus != null) {
      const focusScriptPath = path.join(__dirname, "../../../scripts/focus-chrome.scpt")
      applescript.execFile(focusScriptPath);
    }
  });

}
