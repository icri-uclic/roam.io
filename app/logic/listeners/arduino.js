const config = require('../../../config');
const logButton = require('../log')('press');
const { fadeInLED, fadeOutLED } = require('../arduino/utils');
const eyeCountdown = require('../arduino/eye-countdown');
const eyeRoll = require('../arduino/eye-roll');


/**
  STATE ⟶ ARDUINO
*/


exports.fromState = function(state, arduino) {

  let pulsatingSubmit = false;

  function pulseSubmit() {
    if (!pulsatingSubmit) {
      arduino.submit.led.pulse({
        duration: 1000,
        easing: "inOutSine",
        cuePoints: [0, 1],
        keyFrames: [255, 110]
      });
      pulsatingSubmit = true;
    }
  }

  function stopPulseSubmit() {
    if (pulsatingSubmit) {
      arduino.submit.led.stop().off();
      pulsatingSubmit = false;
    }
  }

  state.on("update:state", (newState) => {

    if (newState === "BEGIN") {
      arduino.bowtie.led.pulse(500);
      const b = config.NEOPIXEL_BRIGHTNESS;
      arduino.eyes.left.color([b,b,b]);
      arduino.eyes.right.color([b,b,b]);
      arduino.eyes.strip.show();
    } else {
      fadeOutLED(arduino.bowtie.led, 250);
    }

    if (newState === "ROLLING_EYES") {
      state.set("question", { text: "" });
      eyeRoll(arduino).then(() => {
        state.set("state", "QUESTION");
      });
    }

    if (newState === "RESPONSE") {
      const question = state.get("originalQuestion");
      // make sure it finishes 250ms earlier so it doesn't override anything else
      let timeout = question.timeout - 250;
      const wait = Math.floor(config.WAIT_BEFORE_COUNTDOWN * 1000);
      if (wait < timeout) setTimeout(() => {
        eyeCountdown(arduino, timeout - wait);
      }, wait);
    }

  });

  state.on("update:answers", (answers) => {

    arduino.answers.forEach(({ led }, index) => {
      if (index < answers.length) {
        fadeInLED(led, 3000);
      } else {
        fadeOutLED(led, 250);
      }
    });

    const index = state.get("keyboard");
    if (index != null) {
      const answer = state.get("answers")[index];
      if (answer.text.length > 0) {
        pulseSubmit();
      } else {
        stopPulseSubmit();
      }
    }
  });

  state.on("update:keyboard", (index) => {
    if (index == null) {
      arduino.keyboard.backlightOff();
      stopPulseSubmit();
    } else {
      arduino.keyboard.backlightOn();
    }
  });

}


/**
  ARDUINO ⟶ STATE
*/

exports.toState = function(state, arduino) {

  arduino.answers.forEach(({ button }, index) => {
    button.on("press", () => {
      logButton(`answer:${index}`);
      const lang = state.get("language");
      let answers = state.get("answers");
      let answer = answers[index];
      if (answer == null) return;
      let originalAnswer = state.get("originalQuestion").answers[index].answer[lang];
      if (state.get("state") !== "QUESTION") {
        return;
      } else if (answer && answer.keyboard) {
        if (originalAnswer === answer.text) {
          answer.text = "";
          state.set("answers", answers);
          state.set("keyboard", index);
        } else if (state.get("keyboard") == index) {
          state.select(index);
        } else {
          state.set("keyboard", index);
        }
      } else if (answer != null) {
        state.select(index);
      }
    });
  });

  arduino.bowtie.button.on("press", () => {
    logButton("bowtie");
    if (state.get("state") === "BEGIN") {
      state.set("state", "ROLLING_EYES");
    }
  });

  arduino.submit.button.on("press", () => {
    logButton("submit");
    const index = state.get("keyboard");
    if (index != null) {
      state.select(index);
    }
  });

  arduino.lang.on("select", (lang) => {
     logButton("language", lang);
    state.set("language", lang);
  })

  arduino.lang.on("deselect", (lang) => {

    //hacky way 
    if(config.LANGUAGES.indexOf(lang) ==1) {
      lang = config.LANGUAGES[0];
    } else {
      lang = config.LANGUAGES[1];
    }

    logButton("language", lang);
    state.set("language", lang);
  });

}
