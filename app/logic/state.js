const _ = require('lodash');
const EventEmitter = require('events').EventEmitter;

class State extends EventEmitter {

  constructor(state={}) {
    super();
    this.setMaxListeners(Infinity);
    this.state = state;
  }

  getState() {
    return _.cloneDeep(this.state);
  }

  get(key) {
    return _.cloneDeep(this.state[key]);
  }

  set(key, changes, silent=false) {
    return this.update({ [key]: changes }, silent);
  }

  update(changes, silent=false) {
    const oldState = this.getState();
    const newState = Object.assign(this.state, changes);
    let somethingChanged = false;
    for (const key in changes) {
      if (!_.isEqual(changes[key], oldState[key])) {
        somethingChanged = true;
        if (!silent) this.emit(
          `update:${key}`,
          changes[key],
          oldState[key]
        );
      }
    }
    if (somethingChanged && !silent) {
      this.emit("update", newState);
    }
  }

  select(index) {
    this.emit("select:answer", index);
  }

  after(eventName, handler) {
    return this.on(eventName, (...args) => {
      setTimeout(() => {
        handler(...args);
      }, 10);
    });
  }

}

module.exports = State;
