const five = require('johnny-five');
const pixel = require('node-pixel');
const RotarySwitch = require('./rotary-switch');
const Ring = require('./ring');
const Keyboard = require('./keyboard');
const config = require('../../../config');
const board = five.Board({ repl: false });

// shorthand function for creating a pull-up button
function button(pin) {
  return new five.Button({
    pin: pin,
    isPullup: true
  });
}

// shorthand function for creating an LED
function led(pin) {
  return new five.Led(pin);
}

module.exports = new Promise(function(resolve, reject) {

  board.on("ready", function() {

    const strip = new pixel.Strip({
      board,
      controller: "FIRMATA",
      strips: [{ pin: 22, length: 24 }], // this is preferred form for definition
    });

    resolve({
      board: board,
      answers: [{
        led: led(6),
        button: button(7)
      }, {
        led: led(4),
        button: button(5)
      }, {
        led: led(2),
        button: button(3)
      }],
      bowtie: {
        led: led(11),
        button: button(12)
      },
      submit: {
        led: led(9),
        button: button(10)
      },
      eyes: {
        // `offset` is the offset from the first Neopixel to the top-most
        // Neopixel of the eyes. Used to make sure eyes are in sync (and Roam.io
        // doesn't appear dizzy).
        // Use `node scripts/eye-offset.js` to find the top-most, and copy the
        // offsets here.
        left: new Ring({ strip, start: 12, end: 24, offset: 2 }),
        right: new Ring({ strip, start: 0, end: 12, offset: 3 }),
        strip
      },
      keyboard: new Keyboard({
        fn: 38,
        upArrow: 36,
        downArrow: 40
      }),
      lang: new RotarySwitch(config.LANGUAGES.map((lang, index) => ({
        label: lang,
        pin: [52,50][index]
      })))
    });
  });

  board.on("fail", function(message) {
    reject(message);
  });

});
