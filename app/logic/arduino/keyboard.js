const five = require("johnny-five");

// Maximum brightness setting -- how many arrow presses it takes to get from
// zero to full backlight brightness.
const MAX_BRIGHTNESS = 12; // actually 9, but just to be safe.

// Milliseconds to wait between pin toggles (LOW → HIGH or HIGH → LOW).
// Needs to be long enough, otherwise keyboard ignores it (because debouncing).
const INTERVAL = 25;

const ON = "ON",
      OFF = "OFF",
      CHANGING = "CHANGING";

class Keyboard {

  constructor({ fn, upArrow, downArrow }) {

    this.fn = new five.Pin(fn);
    this.upArrow = new five.Pin(upArrow);
    this.downArrow = new five.Pin(downArrow);

    // Stores current brightness of keyboard backlight.
    // Setting to max brightness ensures keyboard
    this.brightness = MAX_BRIGHTNESS;

    // Stores the timeout given by `setTimeout`, so that it can be cancelled.
    this.timeout = null;

    // The target brightness.
    this.target = null;

    // The state of the backlight (can be ON, OFF or CHANGING).
    this.state = CHANGING;

    // Helper -- since pins have to toggle (LOW → HIGH or HIGH → LOW) this
    // records which transition needs to be performed next.
    this.tock = true;

    // Ensure backlight is off at the beginning.
    this.backlightOff();
  }

  backlightOn() {
    if (this.state === CHANGING) {
      clearTimeout(this.timeout);
      this.brightness -= 3;
    }
    if (this.state !== ON) {
      this.target = MAX_BRIGHTNESS;
      this._tick();
    }
    return this;
  }

  backlightOff() {
    if (this.state === CHANGING) {
      clearTimeout(this.timeout);
      this.brightness += 3;
    }
    if (this.state !== OFF) {
      this.target = 0;
      this._tick();
    }
    return this;
  }

  _tick() {

    let pin, inc;

    if (this.brightness < this.target) {
      this.state = CHANGING;
      this.downArrow.low();
      pin = this.upArrow;
      inc = +1;
    } else if (this.brightness > this.target) {
      this.state = CHANGING;
      this.upArrow.low();
      pin = this.downArrow;
      inc = -1;
    } else {
      this.state = (this.brightness > 0) ? ON : OFF;
      this.fn.low();
      this.downArrow.low();
      this.upArrow.low();
      return;
    }

    if (this.tock) {
      this.fn.high();
      pin.high();
      this.brightness += inc;
    } else {
      pin.low();
      this.fn.low();
    }

    this.tock = !this.tock;

    this.timeout = setTimeout(this._tick.bind(this), INTERVAL);

  }

}

module.exports = Keyboard;
