exports.fadeInLED = function(led, ms) {
  led.fadeIn(ms, function() {
    led.on();
  });
}

exports.fadeOutLED = function(led, ms) {
  led.fadeOut(ms, function() {
    led.off();
  });
}

exports.clamp = function(low, high) {
  return function(x) {
    return Math.min(Math.max(x, low), high);
  }
}
