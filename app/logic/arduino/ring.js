const { mod } = require('../utils');

class Ring {

  constructor({ strip, start=0, end, offset=0 }) {
    this.strip = strip;
    this.start = start;
    this.end = end;
    this.offset = offset;
    this.length = end - start;
  }

  pixel(index) {
    const i = mod(index + this.offset, this.length) + this.start;
    return this.strip.pixel(i);
  }

  color(...args) {
    this.forEach((pixel) => pixel.color(...args));
    return this;
  }

  off() {
    this.color([0,0,0]);
    return this;
  }

  forEach(fn) {
    for (let i = 0; i < this.length; i++) {
      fn(this.pixel(i), i, this);
    }
    return this;
  }

}

module.exports = Ring;
