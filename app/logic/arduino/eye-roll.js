const config = require('../../../config');
const { mod } = require('../utils');

const BRIGHTNESS = config.NEOPIXEL_BRIGHTNESS;
const TAIL = 8;

module.exports = function(arduino) {
  return new Promise(function(resolve, reject) {

    let index = 3;
    const last = 2;
    const length = 12;

    function run() {
      if (index === last) {
        [arduino.eyes.left, arduino.eyes.right].forEach((eye) => {
          eye.color([BRIGHTNESS, BRIGHTNESS, BRIGHTNESS]);
        });
        arduino.eyes.strip.show();
        resolve();
      } else {
        [arduino.eyes.left, arduino.eyes.right].forEach((eye) => {
          eyerollStep(eye, index);
        });
        arduino.eyes.strip.show();
        index = (index + 1) % 12;
        setTimeout(run, 80);
      }
    }

    function eyerollStep(eye, index) {
      eye.color([BRIGHTNESS, BRIGHTNESS, BRIGHTNESS]);
      for (let i = 0; i < TAIL; i++) {
        let b = i * (1/TAIL);
        if (b < 0) b = 0;
        b = Math.round(b * b * BRIGHTNESS);
        eye.pixel(index-i).color([b,b,b]);
      }
    }

    run();

  });
};
