const config = require('../../../config');
const { mod } = require('../utils');
const { clamp } = require('./utils');

const BRIGHTNESS = config.NEOPIXEL_BRIGHTNESS;

module.exports = function(arduino, duration) {
  return new Promise(function(resolve, reject) {

    const getBrightness = clamp(0, 1);
    const start = Date.now();
      console.log(duration)

    function run() {

      const totalLEDs = 12;
      const bandWidth = 1 / totalLEDs;
      let progress = (Date.now() - start) / duration;
      if (progress > 1) {
        resolve();
        return;
      }

      [arduino.eyes.left, arduino.eyes.right].forEach((eye) => {
        eye.forEach((pixel, index) => {
          const brightness = 1 - getBrightness((progress - (index / totalLEDs)) / bandWidth);
          const color = brightness > 0.5 ? BRIGHTNESS : 0;
          pixel.color([color, color, color]);
        });
      });

      arduino.eyes.strip.show();

      setTimeout(run, 50);

    }

    run();

  });
};
