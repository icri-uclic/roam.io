const five = require("johnny-five");
const EventEmitter = require("events").EventEmitter;

class RotarySwitch extends EventEmitter {

  constructor(pins) {
    super();

    this.pins = pins.map(({ pin, label }) => {
      const button = new five.Button({
        pin: pin,
        isPullup: true
      });
      return { pin, button, label };
    });

    this.pins.forEach(({ button, label }) => {
      button.on("down", () => {
        this.emit("select", label);
      });
      button.on("up", () => {
        this.emit("deselect", label);
      });
    });

  }

}

module.exports = RotarySwitch;
