const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const logDebug = require('debug')('roamio:logging');
const config = require('../../config');
const Response = require('../models/Response');
const questions = require('../questions');
const { datestamp } = require('./utils');

const filename = datestamp() + ".log";

config.RESPONSES_FOLDERS.forEach((folder) => mkdirp.sync(folder));

const files = config.RESPONSES_FOLDERS.map((folder) => {
  return fs.openSync(
    path.resolve(path.join(folder, filename)),
    'a'
  );
});

exports.save = function(data) {
  const json = JSON.stringify(data) + '\n';
  files.forEach((file) => {
    fs.appendFile(file, json, function(error) {
      if (error) return logDebug("Failed to append response to file.", error);
    });
  });

  // FIREBASE
  
  return new Response(data).save(function(error) {
    if (error) return logDebug("Failed to store response in database.", error);
  });
};

exports.getCounts = function(questionId) {
  return new Promise(function(resolve, reject) {

    let counts = {};

    const question = _.find(questions, { id: questionId });

    if (question) {
      question.answers.forEach((answer, index) => {
        counts[index] = 0;
      });
    }

    Response.find({ questionId: questionId }, function(err, responses) {
      if (err) return reject(err);
      Object.assign(counts, _.countBy(responses, "answerIndex"));
      resolve(counts);
    });

  });
};
