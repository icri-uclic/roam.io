const _ = require('lodash');
const error = require('debug')('roamio:error');
const questions = require('../questions');
const config = require('../../config');

const questionsByGroup = _.groupBy(questions, "category");
const groups = _.keys(questionsByGroup);

let previousIds = [];
let currentGroupIndex = 0;
let currentQuestionIndex = 0;
module.exports = function() {

  if(!config.NON_RANDOM_QUESTIONS) {
    // 
    const currentGroup = groups[currentGroupIndex];
    const possibilities = questionsByGroup[currentGroup].filter((question) => {
      return !previousIds.includes(question.id);
    });

    if (possibilities.length < 1) {
      error(`Set a lower non-repeating question limit. Exhausted all possible questions for a non-repeating limit of ${config.NON_REPEAT_LIMIT}.`);
    }

    const question = _.sample(possibilities);

    previousIds.unshift(question.id);
    previousIds = previousIds.slice(0, config.NON_REPEAT_LIMIT);

    currentGroupIndex = (currentGroupIndex + 1) % groups.length;
  } else {

    currentQuestionIndex++;
    currentQuestionIndex %=  questions.length;
    question = questions[currentQuestionIndex];
    
  }

  return question;
};
