exports.eventNames = [
  "update:state",
  "update:language",
  "update:visualisation",
  "update:question",
  "update:answers",
  "update:keyboard",
  "select:answer"
];

exports.stripHTML = function(string) {
  return string.replace(/<\/?[^>]+(>|$)/g, "");
};

exports.mod = function mod(n, m) {
  return ((n % m) + m) % m;
};

exports.datestamp = function() {
  return new Date().toISOString()
    .replace(/T/, '-')
    .replace(/\..+/, '')
    .replace(/:/g, '-');
};
