const debug = require('debug')('roamio:logic');
const State = require('./state');
const arduinoSetup = require('./arduino/setup');

const arduinoSubscribe = require('./listeners/arduino');
const socketSubscribe = require('./listeners/socket');
const stateSubscribe = require('./listeners/state');
const loggingSubscribe = require('./listeners/logging');

function syncState(state, callback) {
  const newState = new State();
  callback(newState);
  newState.update(state.getState());
}

module.exports = (io, firebase) => {

  const state = new State();

  loggingSubscribe(state);
  stateSubscribe(state, firebase);

  state.update({
    state: "BEGIN",
    language: "EN"
  });

  debug("Listening for clients...");

  io.on("connect", function(socket) {
    debug("New client connected.");
    syncState(state, (newState) => {
      socketSubscribe.fromState(newState, socket);
      debug("Socket state synchronised.");
    });
    socketSubscribe.fromState(state, socket);
    socketSubscribe.toState(state, socket);
    debug("Socket is listening.");
  });

  arduinoSetup.then(function(arduino) {
    debug("Arduino connected.");
    syncState(state, (newState) => {
      arduinoSubscribe.fromState(newState, arduino);
      debug("Arduino state synchronised.");
    });
    arduinoSubscribe.fromState(state, arduino);
    arduinoSubscribe.toState(state, arduino);
    debug("Arduino is listening.");
  }).catch(function(error) {
    console.error("Arduino error:", error);
  });
}
