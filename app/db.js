const mongoose = require('mongoose');
mongoose.connect("mongodb://localhost/logs");
const db = mongoose.connection;

module.exports = new Promise(function(resolve, reject) {

  db.on("error", console.error.bind(console, 'connection error:'));
  db.once("open", function() {
    resolve(db);
  });

});
