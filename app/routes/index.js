const _ = require('lodash');
const express = require('express');
const router = express.Router();
const config = require('../../config');
const Response = require('../models/Response');

// Index page, the head of Roam.io, containing the question and visualisation.
router.get("/", function(req, res) {
  res.render("index", { config });
});

// Answers page, containing multiple choice answers for input panel.
router.get("/answers", function(req, res) {
  res.render("answers", { config });
});

// JSON of all the answer responses, used only for debugging currently.
router.get("/responses", function(req, res) {
  Response.find({}, { _id: 0, __v: 0 }, function(err, responses) {
    if (err) return res.status(500).json({ error: err });
    res.json(responses);
  });
});

router.get("/responses/:questionId", function(req, res) {
  Response.find({ questionId: req.params.questionId }, { _id: 0, __v: 0, type: 0 }, function(err, responses) {
    if (err) return res.status(500).json({ error: err });
    res.json(responses);
  });
});

router.get("/stats/:questionId", function(req, res) {
  Response.find({ questionId: req.params.questionId }, function(err, responses) {
    if (err) return res.status(500).json({ error: err });
    res.json({
      questionId: req.params.questionId,
      counts: _.countBy(responses, "answerIndex")
    });
  });
});

// JSON of all the questions (as specified in `/questions` folder).
// Used only for debugging currently.
router.get("/questions", function(req, res) {
  res.json(require('../questions'));
});

module.exports = router;
