const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const yaml = require('js-yaml');
const config = require('../config');

const defaults = {
  visualisation: config.VISUALISATION_DEFAULTS,
  timeout: config.RESPONSE_TIMEOUT
};

function trimExtension(filename) {
  return filename.replace(/\.[^/.]+$/, "");
}

function readQuestions(dir, { ignore=[] }) {

  const filenames = fs.readdirSync(dir);

  return filenames
    .filter((filename) => filename.endsWith(".yml"))
    .filter((filename) => !ignore.includes(filename))
    .map((filename) => {
      console.log(filename)
      const filepath = path.join(dir, filename);
      const content = fs.readFileSync(filepath, "utf8");
      let data = yaml.load(content);
      data = _.defaultsDeep(data, defaults);
      data.category = filename.substring(0, filename.indexOf('-'));
      data.id = trimExtension(filename);
      data.timeout = Math.floor(data.timeout * 1000);
      return data;
    });
}

module.exports = readQuestions(config.QUESTIONS_FOLDER, { ignore: ["template.yml"] });
