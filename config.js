// Default time (in seconds) for response (after answering a question) to stay up.
exports.RESPONSE_TIMEOUT = 4;

// set this to false if you want questions to be sampled randomly from different categories
exports.NON_RANDOM_QUESTIONS = true;


// Prevents repeating the previous X questions. This should be less than the
// total number of questions, otherwise the system will crash.
exports.NON_REPEAT_LIMIT = 5;

// The message shown before pressing the bowtie.
exports.GREETING = {
  EN: "Press my bow tie for another question",
  PT: "Pressione o botão em forma de laço para outra pergunta"
};

// Placeholder text that is shown when answer "Other" is selected.
exports.KEYBOARD_EXPLANATION = {
  EN: "Type your answer",
  PT: "Escreva a sua resposta"
};

// The heading of the responses (what other people answered) visualisation.
exports.RESPONSES_HEADING = {
  EN: "Here's how others responded...",
  PT: "Aqui está o que outros responderam..."
};

// The default response in case no response is specified in the question file.
// Needs to be a function that accepts a percentage (as string) as a parameter.
exports.DEFAULT_RESPONSE = {
  EN: (percent) => `${percent}% of the people answered the same way`,
  PT: (percent) => `${percent}% das pessoas deram a mesma resposta`,
  // If percentage cannot be calculated, the response below is shown.
  // This only happens when there is no responses at all collected.
  DEFAULT: {
    EN: "Thank you",
    PT: "Obrigado"
  }
};

// Character input limit for custom (keyboard) answers.
exports.CHARACTER_LIMIT = 120;

// Language labels (as used in question files)
// Maximum of 4, since we have a 4 step rotary switch.
exports.LANGUAGES = ["EN", "PT"];

// Questions folder where the YAML question files are.
exports.QUESTIONS_FOLDER = "./questions";

// Log folders where responses are backed up. Can be a USB drive.
exports.RESPONSES_FOLDERS = ["./responses"];

// Logs folders
exports.LOGS_FOLDERS = ["./logs"];

// Minimum number of responses for the responses visualisation to be shown.
exports.MINIMUM_RESPONSES = 0;

// The default brightness for the Neopixel eyes. Can be 0–255.
exports.NEOPIXEL_BRIGHTNESS = 5;

// Seconds to wait before starting the "countdown" with the eyes.
exports.WAIT_BEFORE_COUNTDOWN = 2;

// Default properties for visualisation (see a YAML question file).
exports.VISUALISATION_DEFAULTS = {
  loop: false,
  show_responses: true
};
