var socket = io();

/* selectors */
var answersElem = d3.select("#answers");
var answerInput = d3.select("#answer-input");

var answerInputIndex = null;
var placeholder;

answerInput.on("input", function() {
  socket.emit("edit:answer", {
    index: answerInputIndex,
    value: this.value
  });
});

socket.on("update:answers", function(answers) {
  console.log("update:answers", answers);
  var selection = answersElem.selectAll(".answer").data(answers);
  selection.exit()
    .transition()
      .ease(d3.easeCubicOut)
      .duration(1000)
      .style("opacity", 0)
      .on("end", function() {
        d3.select(this).remove();
      });
  selection.enter()
    .append("div")
    .attr("class", "answer")
    .style("opacity", 0)
  .merge(selection)
    .html(function(d) {
      var content = "<span class='caret'>&nbsp;</span>";
      return d.text ? d.text + content : content + placeholder;
    })
    .each(function() { textFit(this, { minFontSize: 32, maxFontSize: 90, multiLine: true, alignVert: true }); })
    .classed("placeholder", function(d) { return d.text === ""; })
    .transition()
      .ease(d3.easeCubicOut)
      .duration(1000)
      .style("opacity", 1);
});

/* shortly highlights selected answer, then makes them all disappear */
socket.on("select:answer", function(index) {
  console.log("select:answer", index);
  answersElem.selectAll(".answer")
    .filter(function(d,i) { return i != index; })
    .transition()
      .ease(d3.easeCubicOut)
      .duration(250)
      .style("opacity", 0)
  answersElem.selectAll(".answer")
    .filter(function(d,i) { return i == index; })
    .transition()
      .ease(d3.easeCubicOut)
      .duration(200)
      .style("color", "white")
      .style("border-color", "white")
    .transition()
      .ease(d3.easeCubicOut)
      .delay(400)
      .duration(200)
      .style("opacity", 0)
      .on("end", function() {
        d3.select(this).style("border-color", "");
      });

});

socket.on("update:keyboard", function(index) {

  answerInputIndex = index;

  answerInput.property("value", "");

  answersElem.selectAll(".answer")
    .classed("active", function(d, i) {
      return index === i;
    })
    .each(function(d, i) {
      if (index === i) {
        answerInput.property("value", d.text);
      }
    });

  var node = answerInput.node();

  if (index != null) {
    node.focus();
    var length = node.value.length;
    node.setSelectionRange(length, length);
  } else {
    node.blur();
  }

});

socket.on("update:placeholder", function(newPlaceholder) {
  placeholder = newPlaceholder;
});

socket.on("reload", function() {
  window.location.reload();
});
