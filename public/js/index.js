var socket = io();

/* selectors */
var visualisationElem = d3.select("#visualisation");
var questionElem = d3.select("#question > h1");

var DISAPPEAR_LENGTH = 200;
var APPEAR_LENGTH = 500;

let timeout = null;
let questionEnded = true;
let questionText;

function questionEnd(elem) {
  questionEnded = true;
  d3.select(elem).html(questionText);
  textFit(elem, {
    minFontSize: 30,
    maxFontSize: 100,
    multiLine: true,
    alignHoriz: true,
    alignVert: true
  });
}

function checkQuestionEnd() {
  timeout = setTimeout(function() {
    if (!questionEnded) {
      questionEnd(questionElem.node());
    }
  }, DISAPPEAR_LENGTH + APPEAR_LENGTH);
}

socket.on("update:question", function({ text }) {
  console.log("update:question", text);
  questionText = text;
  questionEnded = false;
  clearTimeout(timeout);
  questionElem.transition()
    .duration(DISAPPEAR_LENGTH)
    .style("opacity", 0)
    .on("end", function() {
      questionEnd(this);
    })
  .transition()
    .duration(APPEAR_LENGTH)
    .style("opacity", 1);
  checkQuestionEnd();
});

socket.on("update:visualisation", function(visualisation) {
  console.log("update:visualisation", visualisation);

  if (_.isEmpty(visualisation)) {
    // empty it out.
    visualisationElem.transition()
      .duration(DISAPPEAR_LENGTH)
      .style("opacity", 0)
      .on("end", function() {
        d3.select(this).html("").style("opacity", 1);
      });
  } else if (visualisation.type === "responses") {
    visualisationElem.transition()
      .duration(DISAPPEAR_LENGTH)
      .style("opacity", 0)
      .on("end", function() {
        visualisationElem.html("").style("opacity", 1);
        plotResponses(visualisationElem, visualisation);
      });
  } else if (visualisation.type === "video") {
    visualisationElem.html("").append("video")
      .attr("src", "/media/" + visualisation.path)
      .attr("autoplay", true)
      .attr("loop", visualisation.loop ? true : null)
      .style("opacity", 0)
    .transition()
      .delay(DISAPPEAR_LENGTH)
      .duration(APPEAR_LENGTH)
      .style("opacity", 1);
  } else if (visualisation.type === "image") {
    visualisationElem.html("").append("img")
      .attr("src", "/media/" + visualisation.path)
      .style("opacity", 0)
    .transition()
      .delay(DISAPPEAR_LENGTH)
      .duration(APPEAR_LENGTH)
      .style("opacity", 1);
  }
});

socket.on("reload", function() {
  window.location.reload();
});
