function trimLabel(s) {
  if(s.length < 20) return s;
  var n = "";
  s.split(" ").forEach(function(d,i){
    if(n.length<20) {
      if(i>0)n+=" ";
      n+=d;
    }
  });
  return n+"...";
}


function plotResponses(root, response) {

  var DELAY = 1500;
  var HEADING_APPEAR = 1000;
  var STAGGER = 200;
  var WIDTH = 490;
  var HEIGHT = 200;

  var explanation = response.explanation;
  var data = response.data;
  var barsDelay = explanation ? DELAY + HEADING_APPEAR : DELAY;

  var total = _.sum(data.map(function(d) { return d.count }));

  data = data.map(function(d) {
    var fraction = d.count / total;
    return {
      label: trimLabel(d.label),
      fraction: isNaN(fraction) ? 0 : fraction
    }
  });

  var x = d3.scaleLinear()
    .domain([0, 1])
    .range([0, WIDTH]);

  root.append("h1")
    .attr("class", "chart-heading")
    .style("opacity", 0)
    .html(explanation ? response.heading : "")
  .transition()
    .delay(DELAY)
    .duration(HEADING_APPEAR)
    .style("opacity", 1);

  var chart = root.append("div")
    .attr("class", "chart")
    .style("padding-top", explanation ? null : "35px");

  var selection = chart.selectAll(".response").data(data);

  selection.exit().remove();

  var responses = selection.enter()
    .append("div")
    .attr("class", "response")

  responses.style("opacity", 0)
  .transition("a")
    .delay(function(d, i) { return i * STAGGER + barsDelay; })
    .duration(500)
    .style("opacity", 1);

  responses.append("div")
    .attr("class", "label")
    .html(function(d) { return d.label; })
    .each(function() {
      textFit(this, {
        minFontSize: 26,
        maxFontSize: 40,
        multiLine: true,
        alignVert: true
      });
    });

  var bars = responses.append("div")
    .attr("class", "bar")
    .style("width", "0px")
    .style("background-color", "#1baaca")
    .style("color", "rgba(255,255,255,0)");

  bars.append("span")
    .html(function(d) { return (d.fraction * 100).toFixed(0) + "%"; });

  bars.transition("width")
    .duration(750)
    .delay(function(d, i) { return i * STAGGER + barsDelay; })
    .style("width", function(d) { return x(d.fraction) + "px"; })
    .style("color", "rgba(255,255,255,1)");

}
