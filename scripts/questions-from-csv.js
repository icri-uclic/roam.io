const fs = require('fs');
const path = require('path');
const slug = require('slug');
const parse = require('csv-parse');

const reImage = /.+\.(jpe?g|png)$/i;

const content = fs.readFileSync(path.resolve(path.join(__dirname, "..", "questions.csv")), "utf8");

function template({ question, answers, file, timeout }) {

  const answersOutput = answers.map((answer, index) => {
    let content = "";
    if (answer.answer) {
      content += `  - answer:
      EN: "${answer.answer}"
      PT: "Answer 1 in Portuguese"
`;
      if (answer.response) {
        content += `    response:
      EN: "${answer.response}"
      PT: "Response 1 in Portuguese"
`;
      }
      if (index == 2) {
        content += `    keyboard: true`;
      }
    }
    return content;
  }).join("\n");

  let visualisation = "";

  if (file) {
    visualisation += `  # visualisation specification
  # type can be be "video" or "image"
  type: "${ reImage.test(file) ? "image" : "video" }"
  # the path of the image/video relative to the /questions/media directory
  path: "${file}"
  # whether the video loops, can be true or false
  loop: false`;
  }

  return `question:
  EN: "${question}"
  PT: "Question in Portuguese"

answers:

${answersOutput}

visualisation:
${visualisation}
  # whether a chart of responses (what other people said) is shown after a
  # question is answered.
  show_responses: true

${ timeout ? `timeout: ${timeout}` : "" }
`;
}

parse(content, { columns: true }, function(err, data) {
  if (err) return console.error(err);

  let questions = data.map((row) => {
    return {
      category: row["Category"],
      question: row["Question"],
      answers: [{
        answer: row["Answer 1"],
        response: row["Response 1"]
      },{
        answer: row["Answer 2"],
        response: row["Response 2"]
      },{
        answer: row["Answer 3"],
        response: row["Response 3"]
      }],
      file: row["File"],
      timeout: row["Timeout"]
    }
  });

  console.log(data);

  // filter out empty rows
  questions = questions.filter(({ question }) => question);

  questions.forEach((row) => {
    const { category, question } = row;
    const filename = category + "-" + slug(question, { lower: true }) + ".yml";
    fs.writeFile(
      path.resolve(path.join(__dirname, "..", "questions", filename)),
      template(row)
    );
  });

});
