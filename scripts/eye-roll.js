const arduinoSetup = require('../app/logic/arduino/setup');
const { mod } = require('../app/logic/utils');

const LOW_BRIGHTNESS = 5;
const BRIGHTNESS = 10;
const TAIL = 8;

arduinoSetup.then((arduino) => {

  let index = 0;
  let last = 12;

  [arduino.eyes.left, arduino.eyes.right].forEach((eye) => {
    eye.forEach((pixel) => pixel.color([LOW_BRIGHTNESS,LOW_BRIGHTNESS,LOW_BRIGHTNESS]));
  });

  arduino.eyes.strip.show();

  function run() {
    if (index === last) {
      [arduino.eyes.left, arduino.eyes.right].forEach((eye) => {
        eye.forEach((pixel) => pixel.color([LOW_BRIGHTNESS, LOW_BRIGHTNESS, LOW_BRIGHTNESS]));
        // eye.pixel(0).color([LOW_BRIGHTNESS, LOW_BRIGHTNESS, LOW_BRIGHTNESS]);
      });
      arduino.eyes.strip.show();
    } else {
      [arduino.eyes.left, arduino.eyes.right].forEach((eye) => {
        eyerollStep(eye, index);
      });
      arduino.eyes.strip.show();
      // index = (index + 1) % last;
      index++;
      setTimeout(run, 65);
    }
  }

  setTimeout(run, 1500);

}).catch((error) => {
  console.error(error);
});

function eyerollStep(eye, index) {
  eye.forEach((pixel) => pixel.color([BRIGHTNESS, BRIGHTNESS, BRIGHTNESS]));
  for (let i = 0; i < TAIL; i++) {
    let b = i * (1/TAIL);
    if (b < 0) b = 0;
    b = Math.round((b) * (b) * BRIGHTNESS);
    eye.pixel(index-i).color([b,b,b]);
  }
}
