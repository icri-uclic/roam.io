const arduinoSetup = require('../app/logic/arduino/setup');
const keypress = require('keypress');

// make `process.stdin` begin emitting "keypress" events
keypress(process.stdin);

arduinoSetup.then((arduino) => {

  console.log("\nUse the arrow keys to find the right offsets, then copy the configuration into ./app/arduino/logic/setup.js\n");

  let side = "left",
      current = { left: 0, right: 0 },
      offset = { left: arduino.eyes.left.offset, right: arduino.eyes.right.offset };

  // listen for the "keypress" event
  process.stdin.on('keypress', function (ch, key) {
    if (key && key.ctrl && key.name == 'c') {
      process.stdin.pause();
      process.exit();
    } else if (key && key.name == "left") {
      side = "left";
    } else if (key && key.name == "right") {
      side = "right";
    } else if (key && key.name == "up") {
      current[side]++;
    } else if (key && key.name == "down") {
      current[side]--;
    }
    show();
  });

  process.stdin.setRawMode(true);
  process.stdin.resume();

  function show() {
    ["left", "right"].forEach((s) => {
      arduino.eyes[s].off();
      arduino.eyes[s].pixel(0).color([0,0,0]);
      arduino.eyes[s]
        .pixel(current[s] - offset[s])
        .color(s == side ? [30,30,30] : [2,2,2]);
    });
    arduino.eyes.strip.show();
    console.log("offset:", current);
  }

  show();

}).catch((error) => {
  console.error(error);
});
