#!/usr/bin/osascript

-- Sets the answers Chrome window as the active window.

set answersPage to "http://localhost:3000/answers/"

tell application "Google Chrome"

	try
		activate
		set answersWindow to item 0 of (every window whose URL of the active tab of it is answersPage)
		set the index of answersWindow to 1
		delay 0.05
		do shell script "open -a Google\\ Chrome"
	on error

	end try

end tell
