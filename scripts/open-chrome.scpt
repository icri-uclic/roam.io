#!/usr/bin/osascript

-- Ensures the 2 browser windows are open ("index" and "answers").
-- If they are not, it opens them and makes them full-screen.

set indexPage to "http://localhost:3000/"
set answersPage to "http://localhost:3000/answers/"

--
set indexLoaded to false
set answersLoaded to false

-- origin of first screen
set x0 to 0
set y0 to 0

-- origin of second screen
set x1 to 1025
set y1 to 0

-- initial width & height of the windows
set width to 500
set height to 500

tell application "Google Chrome"

	activate

	-- close all windows, except for any showing the index or answers pages

	close (every window whose URL of the active tab of it does not contain indexPage and ¬
		URL of the active tab of it does not contain answersPage)

	-- check if the the browser window is already open:
	--   if it is, refresh the page
	--   otherwise, create a new window and make it full-screen

	try
		set indexWindow to item 0 of (every window whose URL of the active tab of it is indexPage)
		set the URL of the active tab of indexWindow to indexPage -- causes a refresh
		set the index of the indexWindow to 1
	on error
		set indexLoaded to true
		set indexWindow to (make new window with properties {bounds: {x0, y0, width, height}})
		tell indexWindow to set URL of active tab to indexPage
		-- tell application "System Events" to keystroke "f" using {command down, control down}
	end try

	delay 1

	try
		set answersWindow to item 0 of (every window whose URL of the active tab of it is answersPage)
		set the URL of the active tab of answersWindow to answersPage
		set the index of answersWindow to 1
	on error
		set answersLoaded to true
		set answersWindow to (make new window with properties {bounds: {x1, y1, x1 + width, y1 + height}})
		tell answersWindow to set URL of active tab to answersPage
		-- tell application "System Events" to keystroke "f" using {command down, control down}
	end try

	if indexLoaded then
		set the index of indexWindow to 1
		delay 1
		do shell script "open -a Google\\ Chrome"
		delay 1
		tell application "System Events" to keystroke "f" using {command down, control down}
	end if

	-- delay necessary to wait for full-screen animation to complete
	delay 2

	if answersLoaded then
		set the index of answersWindow to 1
		delay 1
		do shell script "open -a Google\\ Chrome"
		delay 1
		tell application "System Events" to keystroke "f" using {command down, control down}
		delay 1
		set the index of indexWindow to 1
		delay 1
		do shell script "open -a Google\\ Chrome"
		delay 1
		tell application "System Events" to keystroke "f" using {command down, control down}
		delay 1
		tell application "System Events" to keystroke "f" using {command down, control down}
	end if

end tell
