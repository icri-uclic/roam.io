const fs = require('fs');
const path = require('path');
const slug = require('slug');
const parse = require('csv-parse');

const reImage = /.+\.(jpe?g|png)$/i;

const sourceFilename = "questions-dual-lang.csv";

function answersTemplate({ EN, PT }) {
  let content = "";
  for (let i = 0; i < EN.answers.length; i++) {
    let ENanswer = EN.answers[i];
    let PTanswer = PT.answers[i];
    if (ENanswer.answer && PTanswer.answer) {
      content += `  - answer:
      EN: "${ENanswer.answer}"
      PT: "${PTanswer.answer}"
`;
      if (ENanswer.response && PTanswer.response) {
        content += `    response:
      EN: "${ENanswer.response}"
      PT: "${PTanswer.response}"
`;
      }
      if (i == 2) {
        content += `    keyboard: true`;
      }
    }
  }
  return content;
}

function visualisationTemplate({ EN, PT }) {
  if (EN.file) {
    return `  # visualisation specification
  # type can be be "video" or "image"
  type: "${ reImage.test(EN.file) ? "image" : "video" }"
  # the path of the image/video relative to the /questions/media directory
  path: "${EN.file}"
  # whether the video loops, can be true or false
  loop: false`;
  } else {
    return "";
  }
}

function template({ EN, PT }) {

  return `question:
  EN: "${EN.question}"
  PT: "${PT.question}"

answers:

${answersTemplate({ EN, PT })}

visualisation:
${visualisationTemplate({ EN, PT })}
  # whether a chart of responses (what other people said) is shown after a
  # question is answered.
  show_responses: true

${ EN.timeout ? `timeout: ${EN.timeout}` : "" }
`;
}

function parseRow(row) {
  return {
    category: row["Category"],
    question: row["Question"],
    answers: [{
      answer: row["Answer 1"],
      response: row["Response 1"]
    },{
      answer: row["Answer 2"],
      response: row["Response 2"]
    },{
      answer: row["Answer 3"],
      response: row["Response 3"]
    }],
    file: row["File"],
    timeout: row["Timeout"]
  };
}

const content = fs.readFileSync(path.resolve(path.join(__dirname, "..", sourceFilename)), "utf8");

parse(content, { columns: true }, function(err, data) {
  if (err) return console.error(err);

  let languages = ["EN", "PT"];
  let langIndex = 0;
  let questions = [];
  let cache = {};

  data.forEach((row) => {
    const d = parseRow(row);
    if (d.question) {
      cache[languages[langIndex]] = d;
      langIndex++;
    } else {
      // if langIndex not 0, means we have a question in cache
      if (langIndex != 0) {
        questions.push(cache);
        cache = {};
      }
      langIndex = 0;
    }
  });

  console.log(questions);

  questions.forEach((row) => {
    const category = row.EN.category;
    const question = row.EN.question;
    const filename = category + "-" + slug(question, { lower: true }) + ".yml";
    fs.writeFile(
      path.resolve(path.join(__dirname, "..", "questions", filename)),
      template(row)
    );
  });

});
