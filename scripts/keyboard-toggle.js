const arduinoSetup = require('../app/logic/arduino/setup');

arduinoSetup.then((arduino) => {

  let a = true;
  setInterval(() => {
    if (a) {
      arduino.keyboard.backlightOn();
    } else {
      arduino.keyboard.backlightOff();
    }
    a = !a;
  }, 2000);

}).catch((error) => {
  console.error(error);
});
