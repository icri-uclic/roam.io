const gulp = require('gulp'),
      nodemon = require('gulp-nodemon'),
      plumber = require('gulp-plumber'),
      livereload = require('gulp-livereload')
      _ = require('lodash');

const debouncedReload = _.debounce(() => livereload.changed(__dirname), 2000);

gulp.task('develop', function () {
  livereload.listen();
  nodemon({
    script: 'bin/www',
    ext: 'js css nunjucks coffee',
    env: { 'DEBUG': 'roamio:*' },
    stdout: true
  }).on('start', function() {
    debouncedReload();
  });
});

gulp.task('default', [
  'develop'
]);
